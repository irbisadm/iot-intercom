var cache_ver = 'v4';
var cacheUrl = [
    '/favicon/android-chrome-192x192.png',
    '/favicon/android-chrome-512x512.png',
    '/favicon/apple-touch-icon.png',
    '/favicon/apple-touch-icon-57x57.png',
    '/favicon/apple-touch-icon-60x60.png',
    '/favicon/apple-touch-icon-72x72.png',
    '/favicon/apple-touch-icon-76x76.png',
    '/favicon/apple-touch-icon-114x114.png',
    '/favicon/apple-touch-icon-120x120.png',
    '/favicon/apple-touch-icon-144x144.png',
    '/favicon/apple-touch-icon-152x152.png',
    '/favicon/apple-touch-icon-180x180.png',
    '/favicon/browserconfig.xml',
    '/favicon/favicon.ico',
    '/favicon/favicon-16x16.png',
    '/favicon/favicon-32x32.png',
    '/favicon/mstile-144x144.png',
    '/favicon/mstile-150x150.png',
    '/favicon/safari-pinned-tab.svg',
    '/js/app.js'
];
self.addEventListener('install',function(event){
    event.waitUntil(
        caches.open(cache_ver).then(function(cache) {
            return cache.addAll(cacheUrl.map(function(item){return new Request(item,{mode:"no-cors"})}));
        }).catch(function (e) {
            console.log(e);
        })
    );
    console.log('[SW] Service worker installed');
});

self.addEventListener('fetch', function(event) {
    var response;
    event.respondWith(caches.match(event.request)
        .then(function(response){
            if(response)
                return response;
            var requestClone = event.request.clone();
            return fetch(requestClone)
                .then(function(response) {
                    if (!response)
                        return response;
                    var responseClone = response.clone();
                    caches.open(cache_ver).then(function(cache) {
                        cache.put(event.request, responseClone);
                    });
                    return response;
                });
        }));
});

self.addEventListener('activate', function(event) {
    event.waitUntil(
        caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if(cache_ver!=key)
                    return caches.delete(key);
            }));
        })
    );
});